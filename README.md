# Twitter Views Button Remover
Twitter's view button really shouldn't be where it is from a UX design perspective. That's what this extension will fix - it removes the views button from the Tweets on your timeline!

## How to Install
Also available as a userscript - get it [here](https://gist.github.com/CominAtYou/7c7edca78183cc0eedf1d09a4c0f011f).

1) Click the green "Code" button above, and select "Download ZIP"
2) Extract the ZIP file.
3) Go to `chrome://extensions`, and enable developer mode.
4) Select "load unpacked", and select the folder you extracted in step 2.

Done!

## Comparison
![](https://user-images.githubusercontent.com/35669235/214481943-d12d96de-269a-4122-96be-3d747586acf0.png)
